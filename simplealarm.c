/* Assignment3: simplealarm.c
 * by Karl Foss and Coda Phillips
 *    Section 1     Section 1
 *
 * This program has an alarm that goes off every 1 second and
 *  in the meantime computes prime numbers until interrupted
 *
 */

#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#define true 1
#define false 0


/* 
 * Handles the signal send by the alarm and prints
 * the current time
 *
 * Parameters:
 *     int sig: signal number 
 */
void handle_alarm(int sig)
{
    /* Declare time variable that will hold the current time from the system */ 
    time_t current_time;
    /* Declare time string to store the human-readable current time */
    char* c_time_string;

    /* Initialize with the current time */
    current_time = time(NULL);

    /* Check time for errors */
    if (current_time == ((time_t) - 1))
    {
        fprintf(stderr,"Error: Can't compute current time\n");
        /* Print strerror */
        fprintf(stderr,"%m\n");
    	exit(1);
    }
    
    /* Converts to the local time format  */
    c_time_string = ctime(&current_time);

    /* Checks to make sure the conversion worked */
    if (c_time_string == NULL)
    {	
        fprintf(stderr,"Error: Can't convert to local time\n");
        fprintf(stderr,"%m\n");
        exit(1);
    }

    /* print the time */
    printf("\nCurrent time is %s", c_time_string);

    /* Resend SIGALRM in 1 second */
    alarm(1);
}




/*
 * main runs an algorithm to find prime numbers in an inefficient manner 
 * Sets itself to be interrupted every second by SIGALRM, and registers a handler
 * Returns 0 if sucess, something else if there was an error
 */
int main()
{
    /* Declare variables for prime work */
    int check;
    int i;

    /* Allocate a sigaction struct to pass to sigaction */
    struct sigaction handler,old;

    /* Set handler to our function */
    handler.sa_handler = handle_alarm;

    /* Remind the user how to send a SIGTERM */
    printf("Enter ^C to end the program:\n");

    /* Make an empty sigset_t or handle the error*/
    if (sigemptyset(&handler.sa_mask))
    {
        fprintf(stderr,"sigemptyset failed\n");
        fprintf(stderr,"%m\n");
        return 1;
    }

    /* Clear flags */
    handler.sa_flags = 0;

    /* Register signal handler or handle the error*/
    if(sigaction(SIGALRM,&handler,&old) == -1)
    {
        fprintf(stderr,"Sigaction failed\n");
        fprintf(stderr,"%m\n");
        return 1;
    }

    /* Set alarm to go off in 1 second */
    alarm(1);

    /* Start prime finding at 1 million */
    check = 1000001;
    int prime;

    /* Do work finding primes */
    while(1)
    {
        prime = true;
        /* Iterate through all possible factors */
        for(i= (check - 1); i>1; i--)
        {
            if((check % i) == 0)
            {
                /* Know it's not prime if you find a factor */
                prime = false;
            }
        }

        /* i == 1 iff we could not find any factors */
        if(prime)
        {
            /* Print the prime */
            printf("%d ",check);
        }

        /* Get ready for the next one */
        check++;
    }
}
