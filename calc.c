/*
 * Assignment3: calc.c
 * by Karl Foss and Coda Phillips
 *    Section 1     Section 1
 *
 * This program runs a simple calculator that will print the number of
 *  operations when terminated, or an error message for a floating point 
 *  error
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#define MAX_STRING_SIZE 50

/* the number of opertions done  */
int count;

/*
 *  handle_fpe prints a message to anyone who tries to
 *  perform a division by zero 
 *
 *  parameters:
 *      sig - the signal number 
 */
void handle_fpe(int sig)
{
    /* Print error and quit */
    printf("You cannot divide by zero\n");
    exit(1);
}




/*
 *  handle_int prints the number of operations done by the calculator before
 *  the program is terminated
 *
 *      parameters:
 *         sig - the signal number
 */
void handle_int(int sig)
{
    /* Print operations and quit */
    printf("There were %d operations done.\n", count);
    exit(1);
}




/*
 * A simple calculator. Prints number of operations
 * performed when sent a SIGTERM.
 *
 */
int main()
{

    /* Holds the user input */
    char input[MAX_STRING_SIZE];
    /* the first operand */
    int first_op;
    /* the second operand */
    int second_op;
    /* the answer to the calculation */
    int answer;

    /* Signal handling structs to hold parameters*/
    struct sigaction fpe,interrupt,old;

    /* Use our handler */
    fpe.sa_handler = handle_fpe;

    /* Make an empty sigset or handle the error */
    if(sigemptyset(&fpe.sa_mask)!=0)
    {
        fprintf(stderr,"Error making empty set\n");
        fprintf(stderr,"%m\n");
        return 1;
    }

    /* Clear the flags */
    fpe.sa_flags = 0;
    
    /* Register the signal handler or handle the error */
    if(sigaction(SIGFPE,&fpe,&old)==-1){
        fprintf(stderr,"Error registering handler\n");
        fprintf(stderr,"%m\n");
        return 1;
    }

    /* Use our handler */
    interrupt.sa_handler = handle_int;

    /* Use an empty sigset or handle the error */
    if(sigemptyset(&interrupt.sa_mask) != 0)
    {
        fprintf(stderr,"Error making empty set\n");
        fprintf(stderr,"%m\n");
        return 1;
    }

    /* Clear the flags */
    interrupt.sa_flags = 0;
    
    /* Register the signal handler or handle the error*/
    if(sigaction(SIGINT,&interrupt,&old) == -1)
    {
        fprintf(stderr,"Error registering handler\n");
        fprintf(stderr,"%m\n");
        return 1;
    }


    /* The calculator */
    while(1)
    {
        /* Get input from the user */
        /* Prompt for and read the first operand */
        printf("Enter one integer value\n");
        if(fgets(input, MAX_STRING_SIZE, stdin) == NULL)
        {
            fprintf(stderr,"Error occured getting input\n");
            fprintf(stderr,"%m\n");
            return 1;
        }
        /* convert the input to an integer  */
        first_op = atoi(input);

        /* Prompt for and read the second operand */
        printf("Enter another integer value\n");
        if(fgets(input, MAX_STRING_SIZE, stdin) == NULL)
        {
            fprintf(stderr,"Error occured getting input\n");
            fprintf(stderr,"%m\n");
            return 1;
        }
        /* convert the input to an integer */
        second_op = atoi(input);

        /* Prompt for  and read the operator to use for the operation */
        printf("Enter an operator\n");
        if(fgets(input, MAX_STRING_SIZE, stdin) == NULL)
        {
            fprintf(stderr,"Error occured getting input\n");
            fprintf(stderr,"%m\n");
            return 1;
        }

        
        /* Calculate the answer */

        /* Addition */
        if(input[0] == '+')
        {
           answer = first_op + second_op;
        }
        /* Subtraction */
        else if(input[0] == '-')
        {
            answer = first_op - second_op;
        }
        /* Multiplication */
        else if(input[0] == '*')
        {
            answer = first_op * second_op;
        }
        /* Division */
        else if(input[0] == '/')
        {
            answer = first_op / second_op;
        }
        /* An invalid operator was entered so Addition is done by default */
        else
        {
             printf("Error: invalid operator, please enter +,-,*, or /\n");
             input[0] = '+';
             answer = first_op + second_op;
        }

        /* Print the operation and answer */
        printf("%d %c %d = %d\n", first_op, input[0], second_op, answer);

        /* One operation was done */
        count++;
    }    
}
