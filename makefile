CC=gcc
CFLAGS=-Wall -m32 -O -g

all: simplealarm calc

simplealarm: simplealarm.c

calc: calc.c

.PHONY:
clean: 
	rm simplealarm
	rm calc

test_simplealarm: simplealarm
	./simplealarm
